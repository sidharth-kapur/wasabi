class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :user
      t.boolean :seen, :default => false
      t.string :text
      t.timestamps
    end
  end
end
