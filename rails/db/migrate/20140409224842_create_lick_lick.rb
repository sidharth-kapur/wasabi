class CreateLickLick < ActiveRecord::Migration
  def change
    create_table :lick_licks do |t|
    	t.references :lick
    	t.references :licker, :class_name => 'User'
    	t.timestamps
    end
  end
end
