class CreateLicks < ActiveRecord::Migration
  def change
    create_table :licks do |t|
      t.belongs_to :sender, :class_name => "User"
      t.belongs_to :receiver, :class_name => "User"
      t.timestamps
    end
  end
end
