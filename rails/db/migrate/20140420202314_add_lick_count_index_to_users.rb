class AddLickCountIndexToUsers < ActiveRecord::Migration
  def change
  	add_index :users, :lick_count
  	add_index :users, :licked_count
  end
end
