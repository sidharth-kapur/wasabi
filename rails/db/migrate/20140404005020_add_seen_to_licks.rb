class AddSeenToLicks < ActiveRecord::Migration
  def change
    add_column :licks, :seen, :boolean
  end
end
