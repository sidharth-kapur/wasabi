class AddIndicesToLickLicks < ActiveRecord::Migration
  def change
  	add_index :lick_licks, :lick_id
  	add_index :lick_licks, :licker_id
  end
end
