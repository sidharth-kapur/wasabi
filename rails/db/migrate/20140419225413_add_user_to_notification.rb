class AddUserToNotification < ActiveRecord::Migration
  def change
  	change_table :notifications do |t|
  		t.references :licker, :class_name => 'User'
  	end
  end
end
