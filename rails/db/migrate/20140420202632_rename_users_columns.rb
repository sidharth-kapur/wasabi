class RenameUsersColumns < ActiveRecord::Migration
  def change
  	change_table :users do |t|
  		t.rename :lick_count, :sent_licks_count
  		t.rename :licked_count, :received_licks_count
  	end
  end
end
