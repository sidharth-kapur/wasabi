class SetDefaultLickCountsOnUsers < ActiveRecord::Migration
  def change
  	change_table :users do |t|
  		change_column :users, :sent_licks_count, :integer, :default => 0
  		change_column :users, :received_licks_count, :integer, :default => 0
  	end
  end
end
