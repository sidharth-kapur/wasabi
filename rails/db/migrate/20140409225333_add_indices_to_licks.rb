class AddIndicesToLicks < ActiveRecord::Migration
  def change
  	add_index :licks, :sender_id
  	add_index :licks, :receiver_id
  end
end
