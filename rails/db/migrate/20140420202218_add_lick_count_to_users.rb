class AddLickCountToUsers < ActiveRecord::Migration
  def change
  	change_table :users do |t|
  		t.integer :lick_count
  		t.integer :licked_count
  	end
  end
end
