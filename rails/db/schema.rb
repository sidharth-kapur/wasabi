# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140420203024) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "achievements", force: true do |t|
    t.string   "type"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lick_licks", force: true do |t|
    t.integer  "lick_id"
    t.integer  "licker_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "lick_licks", ["lick_id"], name: "index_lick_licks_on_lick_id", using: :btree
  add_index "lick_licks", ["licker_id"], name: "index_lick_licks_on_licker_id", using: :btree

  create_table "licks", force: true do |t|
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "seen"
    t.integer  "lick_lick_count", default: 0
  end

  add_index "licks", ["receiver_id"], name: "index_licks_on_receiver_id", using: :btree
  add_index "licks", ["sender_id"], name: "index_licks_on_sender_id", using: :btree

  create_table "notifications", force: true do |t|
    t.integer  "user_id"
    t.boolean  "seen",       default: false
    t.string   "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "licker_id"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.integer  "sent_licks_count",     default: 0
    t.integer  "received_licks_count", default: 0
  end

  add_index "users", ["received_licks_count"], name: "index_users_on_received_licks_count", using: :btree
  add_index "users", ["sent_licks_count"], name: "index_users_on_sent_licks_count", using: :btree

end
