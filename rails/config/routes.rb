Wasabi::Application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  root 'feeds#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  resources :users do
    collection do
      get 'most_licked'
      get 'lick_the_most'
    end
  end

  resources :feeds
  get 'login' => 'login#index'
  post 'login' => 'login#auth'
  get 'logout' => 'logout#log_out'

  resources :licks, :only => :create
  resources :lick_licks, :only => :create
  resources :notifications, :only => :index
  resources :preferences, :only => :index

  # get 'users/most_licked' => 'users#most_licked'
  # get 'users/lick_the_most' => 'users#lick_the_most'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
