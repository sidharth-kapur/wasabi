class LickLick < ActiveRecord::Base
	validates_uniqueness_of :licker, :scope => [:lick]
	validates :licker, presence: true
	validates :lick, presence: true

	after_save :update_lick_lick_count
	belongs_to :licker, :class_name => 'User'
	belongs_to :lick

	private

	def update_lick_lick_count
		self.lick.lick_lick_count += 1
		self.lick.save
	end
end