class ThirdLickAchievement < Achievement
	
	def name
		'Third Lick'
	end

	def self.check(lick)
		user = lick.sender
		puts self.class.name
		if user.sent_licks.size + user.received_licks.size > 2
			ach = ThirdLickAchievement.new
			ach.user = user
			ach.save
			name + ' Achievement earned!'
		end
	end
end