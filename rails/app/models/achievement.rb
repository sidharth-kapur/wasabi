class Achievement < ActiveRecord::Base
	belongs_to :user

	validates_uniqueness_of :user, :scope => :type
	validates :user, presence: true

	# To be implemented by each subclass achievement.
	def self.check
		false
	end
end
