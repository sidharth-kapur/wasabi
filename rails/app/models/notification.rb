class Notification < ActiveRecord::Base

	validates :user, presence: true

	belongs_to :user
	belongs_to :licker, :class_name=>'User'
end
