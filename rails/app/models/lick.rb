class Lick < ActiveRecord::Base
	belongs_to :sender, :class_name=>"User"
	belongs_to :receiver, :class_name=>"User"
	has_many :lick_licks
	after_commit :after_commit

	validates :sender, presence: true
	validates :receiver, presence: true

	def after_commit
		sender.update_lick_count
		receiver.update_lick_count
		check_achievements
	end

	def check_achievements
		puts 'chekcing achievmenets!'
		p Achievement.descendants

		achievements = []
		Achievement.descendants.each do |achievement|
			notice = achievement.check(self)
			if notice
				Notification.create(:text=>notice, :user=>sender)
			end
		end
		achievements
	end

	def create_notification
		Notification.create(:text=>"{sender.username} licked you!", :user=>receiver, :licker=>sender)
	end
	
	def get
		sender = self.sender
		receiver = self.receiver
		{
			:sender => {
				:id => sender.id,
				:username => sender.username
			},
			:receiver => {
				:id => receiver.id,
				:username => receiver.username
			},
			:created_at => self.created_at
		}
	end
end
