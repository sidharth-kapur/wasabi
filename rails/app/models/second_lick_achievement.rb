class SecondLickAchievement < Achievement
	
	belongs_to :user
	
	def name
		'Second Lick'
	end

	def self.check(lick)
		user = lick.sender
		puts self.class.name
		if user.sent_licks.size + user.received_licks.size > 1
			ach = SecondLickAchievement.new
			ach.user = user
			ach.save
			name + ' Achievement earned!'
		end
	end
end