class User < ActiveRecord::Base
	has_many :sent_licks, class_name: "Lick", foreign_key: "sender_id"
	has_many :received_licks, class_name: "Lick", foreign_key: "receiver_id"
	has_many :achievements
	has_secure_password

	def update_lick_count
		self.sent_licks_count = Lick.where(sender_id: id).count
		self.received_licks_count = Lick.where(receiver_id: id).count
		self.save!
	end
end
