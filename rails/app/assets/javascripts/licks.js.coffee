# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on "page:change", ->
	$('.create-lick-lick').each ->
		this.onclick = () ->
			data = {
				lick_lick: {
					lick_id: $(this).attr('data-lick-id'),
				}
			}
			obj = $(this).closest('.lick')
			$.ajax({
				type:'POST',
				url:'/lick_licks/',
				data: data,
				success: (data) ->
					obj.replaceWith(data)
					
				error:(data) ->
					alert 'fail'
			})