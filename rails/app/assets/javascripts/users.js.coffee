# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on "page:change", ->
	if ($('body').attr('data-view') == 'users#index')
		$('#users-tab').addClass('active')
	else if ($('body').attr('data-view') == 'users#show' && $('#user-id').attr('value') == 'true')
		$('#profile-tab').addClass('active')

	$('.create-lick').each () ->
		this.onclick = () ->
			data = {
				lick: {
					receiver_id: $(this).attr('data-user'),
				}
			}
			oldButton = $(this)
			$.ajax({
				type:'POST',
				url:'/licks/',
				data: data,
				success:(data) ->
					$('#licks').prepend(data)
					oldButton.replaceWith('<button class="btn btn-block btn-large btn-disabled">Licked!</button>')
				error:(data) ->
					return #alert 'fail'
			})