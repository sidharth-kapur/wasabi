class LoginController < ApplicationController
	def index
		@user = User.new
	end

	def auth
		user = User.find_by_username(login_params[:username]).try(:authenticate, login_params[:password])
		if user
			session[:user_id] = user.id
			redirect_to feeds_path
		else
			flash[:notice] = "Login failed."
			redirect_to login_path
		end
	end

	def log_out
		session[:user_id] = nil
	end

	private
	def login_params
		params.require(:user).permit(:username, :password)
	end
end
