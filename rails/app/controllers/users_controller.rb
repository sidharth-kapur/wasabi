class UsersController < ApplicationController
	respond_to :json

	def show
		@user = User.find(params[:id])
		@licks = Lick.where('sender_id = ? OR receiver_id = ?', params[:id], params[:id]).order(created_at: :desc).limit(100)

		@sent_count = @user.sent_licks_count
		@received_count = @user.received_licks_count
		@ratio = @received_count.fdiv(@sent_count)
		#@licks = @user.sent_licks + @user.received_licks

		#@licks = Lick.all(:include => [:sender, :receiver, :lick_licks], :order => 'created_at DESC' )

		# respond_with @user.get_profile
	end

	def new
		@user = User.new
	end

	def index
		@users = User.all
	end

	def create
		@user = User.create(user_params)
		if @user.save
			flash[:notice] = 'Account created!'
			session[:user_id] = @user.id
			redirect_to feeds_path
		else
			render 'new', :notice => 'Bad username/bassword!'
		end
	end

	def most_licked
		@users = User.all.order(received_licks_count: :desc)
	end

	def lick_the_most
		@users = User.all.order(sent_licks_count: :desc)
	end

	def user_params
		params.require(:user).permit(:username, :password, :password_confirmation)
	end
end
