class LicksController < ApplicationController
	def create
		@lick = Lick.new(licks_params)
		@lick.sender_id = session[:user_id]
		@lick.save

		ans = @lick.check_achievements
		if ans
			flash[:notice] = ans[0]
			p flash
		end

		render @lick
	end

	private
	def licks_params
		params.require(:lick).permit(:receiver_id)
	end
end
