class NotificationsController < ApplicationController
	def index
		@notifications = Notification.where(user_id: session[:user_id]).limit(20).order('id desc')
		@notifications.each do |notification|
			notification.seen = true
		end
	end
end
