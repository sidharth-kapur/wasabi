class LickLicksController < ApplicationController
	def create
		@lick_lick = LickLick.new(lick_licks_params)
		@lick_lick.licker_id = session[:user_id]
		@lick_lick.save
		render @lick_lick.lick
	end

	private
	def lick_licks_params
		params.require(:lick_lick).permit(:lick_id)
	end
end
