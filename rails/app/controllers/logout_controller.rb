class LogoutController < ApplicationController
	def log_out
		session[:user_id] = nil
		redirect_to login_path
	end
end
