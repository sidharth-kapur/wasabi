Ext.define('wasabi.controller.Main', {
    extend: 'Ext.app.Controller',
    
    stores: ['FeedStore'],

    config: {
        refs: {
            mainView: 'main', 
            btnPrint: 'main button[action=print]',
            btnBack: 'main button[action=back]',

            name: 'main textfield[name=name]',
            printed : 'main textfield[name = printed]'

        },
        control: {
            'btnPrint': {
                tap: 'print'
            },
            'btnBack': {
                tap: 'onBackBtnTap'
            }
        }
    },
    
    //called when the Application is launched, remove if not needed
    launch: function(app) {

    }
});
