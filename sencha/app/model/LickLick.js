Ext.define('LickLick', {
	extend: 'Ext.data.Model',
	config: {
		fields: [
			{name: 'createdAt', type: 'string'}
		],
		belongsTo: ['Lick', 'User']
	}
});