Ext.define('Lick',
{
	extend: 'Ext.data.Model',
	config: {
		fields: [
			{name: 'created_at', type: 'string'}
		],
		belongsTo: 'User',
		hasMany: 'LickLick'
	}
})