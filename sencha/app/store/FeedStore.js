Ext.define('wasabi.store.FeedStore', {
			// type: 'tree',
		extend: 'Ext.data.Store',
		

		config: {
			autoLoad: true,

			fields: [
				{
					name: 'sender', 
					mapping: 'sender.username'
				},
				{
					name: 'receiver', 
					mapping: 'receiver.username'
				},
				'created_at',
				{name: 'leaf', defaultValue: true}
			],

			proxy: {
				type: 'ajax',

				url: 'http://localhost:3000/feeds/',
				reader: {
					type: 'json'
				}
			}
		}
		
		// To use sample data, uncomment 'data' and comment 'proxy'
		/*
		data: [
			{
				"created_at": "2014-04-04T20:06:20.303Z",
				"sender": {
					"username": "sidharth"
				},
				"receiver": {
					"username": "isaac"
				}
			},
			{
				"created_at": "2014-04-04T20:06:27.367Z",
				"sender": {
					"username": "isaac"
				},
				"receiver": {
					"username": "sidharth"
				}
			}
		],
		*/

		
})