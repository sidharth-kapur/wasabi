Ext.define('wasabi.store.ProfileStore', {
		type: 'tree',
		extend: 'Ext.data.Store',
		
		userId: null,

		config: {
			autoLoad: true,
			/*
			fields: [
				'name',
				{
					name: 'sentLicks', 
					mapping: 'sent_licks'
				},
				{
					name: 'receivedLicks', 
					mapping: 'received_licks'
				},
				//'created_at',
				{name: 'leaf', defaultValue: true}
			],*/
			model: 'User',

			proxy: {
				type: 'ajax',

				url: 'http://localhost:3000/users/' + this.userId.toString(),
				reader: {
					type: 'json'
				}
			},

			load: function (obj, records, successful, operation, eOpts) {
				console.log('loaded!');
				console.log(successful);
				console.log(records);
			}
		}
})