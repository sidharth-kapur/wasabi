Ext.define('wasabi.view.Main', {
	extend: 'Ext.tab.Panel',
	xtype: 'main',
	requires: [
		'Ext.TitleBar',
		'Ext.Video',
		'Ext.dataview.List',
		'Ext.data.TreeStore'
	],
	config: {
		tabBarPosition: 'bottom',

		items: [
			{ xclass: 'wasabi.view.Profile' },
			{ xclass: 'wasabi.view.Feed' }
		]
			
		
	}
});
