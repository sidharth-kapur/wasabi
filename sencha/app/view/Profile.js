
// Hardcoded for testing.
var userId = '1';

Ext.define('wasabi.view.Profile', {
	extend: 'Ext.dataview.DataView',
	controller: 'wasabi.controller.Profile',

	config: {
		title: 'Profile',
		iconCls: 'user',
		xtype: 'profile',
		styleHtmlContent: true,
		scrollable: null,
		itemTpl: '{name}!',
		layout: 'vbox',

		items: [
			{
				docked: 'top',
				xtype: 'titlebar',
				title: 'Profile!!'
			},
			{
				xtype: 'dataview',
				itemTpl: '{name}!!',
				flex: '1',
				scrollable: null,
				store: Ext.getStore('wasabi.store.ProfileStore', {userId: 1} )
			},
			{
				xtype: 'list',
				itemTpl: '{sender} licked {receiver}',
				/*data: [
					{name: 'sid'},
					{name: 'eric'},
					{name: 'han'}
				],*/
				flex: '4',
				store: Ext.getStore('wasabi.store.ProfileStore',  {userId: 1} )
				/*
				store: {
					xclass: 'wasabi.store.ProfileStore',
					userId: 1,
					config: {
						fields: [
							{
								name: 'sender',
								mapping: 'sentLicks.sender.username'
							},
							{
								name: 'receiver',
								mapping: 'sentLicks.receiver.username'
							},
							{name: 'leaf', defaultValue: true}
						],
						proxy: {
							reader: {
								type: 'json',
								rootProperty: 'sent_licks'
							}
						}
					}
				}
				*/
			}
		],

		store: Ext.create('wasabi.store.ProfileStore', {userId: 1})
	}
});
