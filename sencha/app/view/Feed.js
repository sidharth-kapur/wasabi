Ext.define('wasabi.view.Feed', {
	extend: 'Ext.dataview.List',
	xtype: 'feed',
	
	config: {
		title: 'Feed',
		iconCls: 'home',
		xtype: 'list',
		styleHtmlContent: true,
		scrollable: true,
		displayFeed: 'created_at',
		itemTpl: '{sender} licked {receiver} at {created_at}!',
		items: {
			docked: 'top',
			xtype: 'titlebar',
			title: 'Feed!!'
		},

		store: Ext.create('wasabi.store.FeedStore')
	}
});
