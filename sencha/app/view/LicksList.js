Ext.define('wasabi.view.LicksList', {
	extend: 'Ext.dataview.List',
	xtype: 'lickslist',

	config: {

		itemTpl: '{sender} licked {receiver} at {created_at}!',

	}
});