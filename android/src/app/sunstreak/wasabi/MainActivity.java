package app.sunstreak.wasabi;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	    WebView web = (WebView)findViewById(R.id.webView1);
	    web.getSettings().setRenderPriority(RenderPriority.HIGH);
	    web.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
	    web.getSettings().setJavaScriptEnabled(true);
	    final Context x = this;
	    web.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
            	final boolean show = true;
            	final int shortAnimTime = getResources().getInteger(
    					android.R.integer.config_mediumAnimTime);
                //hide loading image
            	Animation in = AnimationUtils.loadAnimation(x, R.anim.slide_in_down);
            	final ImageView logo = (ImageView)findViewById(R.id.imageLoading1);
            	final WebView web = (WebView)findViewById(R.id.webView1);
            	logo.animate().setDuration(shortAnimTime)
    			.alpha(show?0:1)
    			.setListener(new AnimatorListenerAdapter() {
    				@Override
    				public void onAnimationEnd(Animator animation) {
    					web.animate().setDuration(shortAnimTime)
    	    			.alpha(show ? 1:0)
    	    			.setListener(new AnimatorListenerAdapter() {
    	    				@Override
    	    				public void onAnimationEnd(Animator animation) {
    	    					web.setVisibility(View.VISIBLE);
    	    				}
    	    			});
    					logo.setVisibility(View.GONE);
    					logo.animate().setDuration(0)
    					.translationY(0);
//    					web.setVisibility(View.VISIBLE);
    				}
    			});
            	
    			

                
                
                
                
                
                
                //show webview
            }


        }); 
	    web.loadUrl("http://192.168.1.5:3000/feeds");
//	    getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
//	    getActionBar().hide();
//	    setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
